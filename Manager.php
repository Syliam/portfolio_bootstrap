<?php 
class Manager
{   
    //crée un nouvel utilisateur en bdd
    public function saveNewUser($user)
    {
        $sql ="INSERT INTO users (nom, prenom, metier, entreprise, msg) VALUES (?, ?, ?, ?, ?)";
        //on utilise la classe DbConnection pour récupérer
        //notre connection à la base de données ($pdo) 
        //pour appeler les méthodes statiques ! 
        $pdo = DbConnection::getPdo();

        //envoie la requête au serveur sql
        $stmt = $pdo->prepare($sql);

        //exécute la requete SQL avec les données users
        $parameters = [
            $user->getNom(), 
            $user->getPrenom(), 
            $user->getMetier(), 
            $user->getEntreprise(), 
            $user->getMessage()
        ];
        $stmt->execute($parameters);
        /*
        $stmt->execute([
            ":nom" => $user->getNom(),
            ":prenom" => $user->getPrenom(),
            ":metier" => $user->getMetier(),
            ":entreprise" => $user->getEntreprise(),
            ":msg" => $user->getMessage(),
        ]);*/
    }

    public function viewRecommandation()
    {

        $sql = "SELECT * FROM users";
        $pdo = DbConnection::getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([]);

        $recommandation = $stmt->fetchAll();
        return $recommandation;
    }
    
}
?>