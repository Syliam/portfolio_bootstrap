<?php 
//récupération information Users

class User
{
    private $id; 
    private $nom;
    private $prenom; 
    private $metier;
    private $entreprise; 
    private $message;

    //id
    public function getId()
    {
        return $this->id;
    }

    //Nom
	public function getNom(){
		return $this->nom;
	}
	public function setNom($nom){
		$this->nom = $nom;
    }
    
    //Prenom
	public function getPrenom(){
		return $this->prenom;
	}
	public function setPrenom($prenom){
		$this->prenom = $prenom;
    }
    
    //Metier
	public function getMetier(){
		return $this->metier;
	}
	public function setMetier($metier){
		$this->metier = $metier;
    }
    
    //Entreprise
	public function getEntreprise(){
		return $this->entreprise;
	}
	public function setEntreprise($entreprise){
		$this->entreprise = $entreprise;
    }
    
    //Message
	public function getMessage(){
		return $this->message;
	}
	public function setMessage($message){
		$this->message = $message;
    }
}

?>