<?php 

include("User.php");


class Controller
{
    //apppelle la vue content
    /**
     * appelle la fonction contactFormHasErrors
     */
    public function accueil()
    {
        if(!empty($_POST)){
            $utils = new Tools();

            $nom = strip_tags($_POST['nom']);
            $prenom = strip_tags($_POST['prenom']);
            $email = strip_tags($_POST['email']);
            $msg = strip_tags($_POST['msg']);
            
            $errors = $utils->contactFormHasErrors($nom, $prenom, $email, $msg);
        }
        include("templates/content.php");
    }

    //apppelle la vue 404
    public function error()
    {
        $utils= new Tools; 
        $utils->refresh();
        include("templates/404.php");
    }

    public function internalError()
    {
        $utils= new Tools; 
        $utils->refresh();
        include("templates/500.php");
    }

    //traitement formulaire de recommandation
    public function recommandation()
    {   
        $utils = new Tools();
        $manager = new Manager();

        if(!empty($_POST)){

            $nom = strip_tags($_POST['nom']);
            $prenom = strip_tags($_POST['prenom']);
            $metier = strip_tags($_POST['metier']);
            $entreprise = strip_tags($_POST['entreprise']);
            $msg = strip_tags($_POST['msg']);
            
            //$errors : ne pas changer le nom de la variable car elle est présente dans le template recommandation
            $errors = $utils->recommandationFormHasErrors($nom, $prenom, $metier, $entreprise, $msg);
            $formIsValid = empty($errors);

            //s'il n'y a pas d'erreur, le formulaire est valide
            if($formIsValid){
                
                //creation et affectation des données utilisateur
                $user = new User();
                $user->setNom($nom);
                $user->setPrenom($prenom);
                $user->setMetier($metier);
                $user->setEntreprise($entreprise);
                $user->setMessage($msg);
                try{
                    $manager->saveNewUser($user);
                }
                catch(Exception $e){
                    $this->internalError();
                }
                
            }
        }
        //$result est utilisé dans le template recommandation
        $results = $manager->viewRecommandation();
        include("templates/recommandation.php");
    }

    public function viewRecommandation()
    {

    }


}
?>