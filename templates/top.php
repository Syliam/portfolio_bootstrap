<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Portfolio d'une étudiante en développement web.fr">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href ="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/mbd.css">
    <script src="https://kit.fontawesome.com/74c1aee90c.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/style.css">

    <!-- Polices -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Allerta&family=Roboto:wght@100&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Allerta&family=Monoton&family=Righteous&family=Roboto:wght@100&display=swap" rel="stylesheet">



    
    <link rel="shortcut icon" href="./images/logo.png">

    <title>Portfolio</title>
</head>

<body id="haut">
    <!-- Navbar--> 
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <a class="navbar-brand" href="#haut">
            <img src="./images/logo1.png" alt="logo"> Maïlys Renaud-Faugère
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#haut">Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#parcours">Parcours et Compétences</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#projets">Projets</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?page=recommandation">Recommandations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Contact</a>
                </li>
            </ul>
        </div>
    </nav>