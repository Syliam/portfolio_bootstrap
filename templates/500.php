<!DOCTYPE html>
<html lang="en">
<!-- Bootstrap CSS -->

<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" href=".css/mbd.css">
<script src="https://kit.fontawesome.com/74c1aee90c.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/style.css">

<!-- Polices -->
<link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">


<link rel="shortcut icon" href="./images/logo.png">

<head">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portfolio</title>
</head">

<body>
<div class="container-fluid presentation">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 text-center pt-5">
                    <h1>Erreur interne</h1>
                    <h2>
                        <i class="fas fa-directions"></i> Vous allez être redirigé vers l'accueil...
                    </h2>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

