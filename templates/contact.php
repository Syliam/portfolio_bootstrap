<!--Contact-->
<div class="container-fluid contact" id="contact">
    <div class="container contact">
    <div class="row">
        <div class="col-lg margin-auto">
            <h2 class="my-3">Contact</h2>
            <form method="post" class="contacts">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" placeholder="Email" name="email">
                </div>
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" placeholder="Nom" name="nom">
                </div>
                <div class="form-group">
                    <label for="prenom">Prénom</label>
                    <input type="text" class="form-control" placeholder="Prénom" name="prenom">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" placeholder="Saisir votre message" rows="3" name="msg"></textarea>
                </div>
                <?php
                //affiche les éventuelles erreurs de validations
                if (!empty($errors)) {
                    echo '<div class="alert alert-danger">';
                    foreach ($errors as $error) {
                        echo '<div>' . $error . '</div>';
                    }
                    echo '</div>';
                }
                ?>
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </form>
        </div>
    </div>
</div>
</div>




