<?php include("top.php") ?>

<!-- Header-->
<header>
    <div class="container-fluid presentation">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 texte">
                    <div class="titre">Etudiante  en  Développement  web</div>
                    <br>
                    <blockquote class=" mt-5">Après plusieurs années d'études et quelques expériences professionnelles
                        dans le domaine de la biologie et de la santé,
                        j'ai décidé de me diriger vers un domaine pour lequel je me suis découvert un grand intérêt.
                        Je suis actuellement en première année de Développement Web.</blockquote>
                    <blockquote>Afin de poursuivre mon cursus, je suis à la recherche d'un contrat de
                        professionnalisation pour septembre 2020 pour une durée de deux ans.</blockquote>
                </div> 
            </div>
        </div>
    </div>
    </header>

<div class="fond">

    <!--Parcours-->
    <div class="container">
        <div class="container-fluid" id="parcours">
            <h2 class="pt-9 mb-5 rounded shadow ">
                <i class="fas fa-graduation-cap mt-5"></i> Formations
            </h2>
            
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card mb-5 rounded shadow">
                        <h5 class="card-header bg-dark text-white">2019 - 2022</h5>
                        <div class="card-body">
                            <h5 class="card-title">Campus Academy</h5>
                            <p class="card-text">Formation en Développement Web à Nantes</p>
                        </div>
                    </div>
                    <div class="card mb-5 rounded shadow">
                        <h5 class="card-header bg-dark text-white">2016 - 2018</h5>
                        <div class="card-body">
                            <h5 class="card-title">Institut en Soins Infirmiers</h5>
                            <p class="card-text">IFSI de Niort</p>
                        </div>
                    </div>
                    <div class="card mb-5 rounded shadow">
                        <h5 class="card-header bg-dark text-white">2014 - 2015</h5>
                        <div class="card-body">
                            <h5 class="card-title">Année de PACES</h5>
                            <p class="card-text">Université de Nantes</p>
                        </div>
                    </div>
                    <div class="card bg-white mb-5 rounded shadow">
                        <h5 class="card-header bg-dark text-white">2012 - 2014</h5>
                        <div class="card-body">
                            <h5 class="card-title">BTS Analyses de Biologie Médicale</h5>
                            <p class="card-text">Lycée Jean Perrin à Rezé</p>
                        </div>
                    </div>
                    <div class="card  bg-white mb-5 rounded shadow">
                        <h5 class="card-header bg-dark text-white">2012</h5>
                        <div class="card-body">
                            <h5 class="card-title">Bac STL option Biochimie Génie Biologique</h5>
                            <p class="card-text">Lycée Talensac à Nantes</p>
                        </div>
                    </div>
                </div>
            </div>

            <!--Expériences-->
            <h2 class="pt-9 mb-5 rounded shadow ">
                <i class="fas fa-briefcase"></i> Expériences
            </h2>

            <div class="card bg-white mb-5">
                <div class="card-body fond">
                    <dl class="row mt-3 pt-9  rounded shadow">
                        <dt class="col-sm-3 bg-dark text-white">
                            <h5>2016 - 2018 </h5>
                        </dt>
                        <dd class="col-sm-9">
                            <div class="font-weight-bold"> Stages infirmiers :</div>
                            L'hôpital de Niort ainsi que dans d'autres
                            structures de soins
                        </dd>

                        <dt class="col-sm-3 bg-dark text-white">
                            <h5>2013 - 2014 </h5>
                        </dt>
                        <dd class="col-sm-9">
                            <div class="font-weight-bold"> Stage 2ème Année de BTS :</div> Laboratoire de la
                            Polyclininque
                            de
                            l'Atlantique
                        </dd>
                        <dt class="col-sm-3 bg-dark text-white">
                            <h5>2012 - 2013 </h5>
                        </dt>
                        <dd class="col-sm-9 ">
                            <div class="font-weight-bold "> Stage 1ère Année de BTS :</div> CHU de Nantes
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

    <!--Compétences-->
    <?php include("competences.php") ?>

    <!--Projets-->
    <?php include("projets.php") ?>

    <!--Formulaire de contact-->
    <?php include("contact.php") ?>

    <!--Footer-->
    <?php include("bottom.php") ?>

</div>