<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Portfolio d'une étudiante en développement web.fr">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/74c1aee90c.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/style.css">

    <!-- Polices -->
    <link href="https://fonts.googleapis.com/css2?family=Allerta&family=Roboto:wght@100&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Allerta&family=Monoton&family=Righteous&family=Roboto:wght@100&display=swap" rel="stylesheet">


    <link rel="shortcut icon" href="./images/logo.png">

    <title>Portfolio</title>
</head>

<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <a class="navbar-brand" href="#haut">
        <img src="./images/logo1.png" alt="logo"> Maïlys Renaud-Faugère
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php?page=accueil">Retour à l'accueil</a>

            </li>
        </ul>
    </div>
</nav>
<div class="fond">
    <div class="container mb-5 " style="margin-top: 8em;">
        <div class="container-fluid">
            <?php foreach ($results as $result) { ?>
            <div class="card result">
                <div class="badge badge-dark">
                    <h5><?php echo $result['nom'] . ' ' . $result['prenom']; ?></h5>
                </div>
                <div class="card-body" style="background-color: #f8f9fa">
                    <p class="card-text"><?php echo $result['msg']; ?></p>
                    <small><?php echo $result['metier'] . ' ' . 'à' . ' ' . $result['entreprise']; ?></small>
                </div>
            </div>
            <br>
            <?php } ?>
        </div>
    </div>
</div>


<!--Formulaire de recommandations-->
<div class="container fluid recommandations">
    <div class="row justify-content-md-center">
        <div class="col col-lg-2">
        </div>
        <div class="col-lg-auto formulaire">

            <div>
                <form method="post">
                    <div class="form-group">
                        <label for="nom">Nom</label>
                        <input type="text" class="form-control" name=nom placeholder="Nom">
                    </div>
                    <div class="form-group">
                        <label for="prenom">Prénom</label>
                        <input type="text" class="form-control" name=prenom placeholder="Prénom">
                    </div>
                    <div class="form-group">
                        <label for="entreprise">Entreprise</label>
                        <input type="text" class="form-control" name=entreprise placeholder="Entreprise">
                    </div>
                    <div class="form-group">
                        <label for="metier">Métier</label>
                        <input type="text" class="form-control" name=metier placeholder="métier">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Message</label>
                        <textarea class="form-control" name=msg placeholder="Saisir votre message" rows="3"></textarea>
                    </div>
                    <?php
                    //affiche les éventuelles erreurs de validations
                    if (!empty($errors)) {
                        echo '<div class="alert alert-danger">';
                        foreach ($errors as $error) {
                            echo '<div>' . $error . '</div>';
                        }
                        echo '</div>';
                    }
                    ?>
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                </form>
            </div>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
</div>




<?php include("templates/bottom.php")
?>