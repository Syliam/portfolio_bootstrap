
<!-- class="page-footer font-small blue -->
<footer class=" d-flex justify-content-center footer">
        <div class="row">
            <ul class="list-unstyled text-small">
                <li><i class="fab fa-gitlab fa-1x" style="padding: 1rem; text-align:left"></i><a style="color:white" href="https://gitlab.com/Syliam/portfolio_bootstrap.git">Gitlab</a></li>
                <li><i class="fab fa-linkedin fa-1x" style="padding: 1rem"></i><a style="color:white" href="https://www.linkedin.com/in/ma%C3%AFlys-renaud-faug%C3%A8re-a64396199/">Linkedin</a></li>
                <li>&copy; <?= date("Y") ?> All rights reserved<a href="#" style="color:white;font-weight:500; padding:1rem;"></a></li>
            </ul>
        </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->
<!--ferme la div class container-->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>

</html>