<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <div class="container-fluid" id="projets">
    <div class="container">
      <h2 class="pt-9 mb-5 rounded shadow ">
        <i class="fas fa-book-open"></i> Projets
      </h2>
      <div class="row mt-5">
        <div class="col-12">

          <!--Card des projets--->
          <div class="card w-75 rounded shadow bg-dark mb-5">
            <div class="card-body">
              <h3 class="card-title text-white">Speedrun</h3>
              <div class="text-center">
                <img src="./images/speedrun.jpg" class="w-100 photo">
              </div>

              <p class="mt-5">
                <a class="btn btn-dark" data-toggle="collapse" href="#collapseExample" role="button"
                  aria-expanded="false" aria-controls="collapseExample">
                  <i class="fas fa-chevron-circle-down"></i>
                </a>
              </p>
              <div class="collapse" id="collapseExample">
                <div class="card card-body">
                  Création d'un jeu vidéo en 2D avec le logiciel Unity. Le language C# a été utilisé.
                </div>
              </div>
            </div>
          </div>

          <div class="card w-75 rounded shadow bg-dark mb-5">
            <div class="card-body">
              <h3 class="card-title text-white">Librairie</h3>
              <div class="text-center">
                <img src="./images/library.PNG" class="w-100 photo">
              </div>

              <p class="mt-5">
                <a class="btn btn-dark" data-toggle="collapse" href="#collapseEx" role="button" aria-expanded="false"
                  aria-controls="collapseExample">
                  <i class="fas fa-chevron-circle-down"></i>
                </a>
              </p>
              <div class="collapse" id="collapseEx">
                <div class="card card-body">
                  Etant passionnée de livres, j'ai voulu créer ma bibliothèque en ligne. J'ai utilisé le framework
                  Angular.
                </div>
              </div>
            </div>
          </div>

          <div class="card w-75 rounded shadow bg-dark mb-5">
            <div class="card-body">
              <h3 class="card-title text-white">Plus ou moins</h3>
              <div class="text-center">
                <img src="./images/jeu_plus_moins.PNG" class="w-100 photo">
              </div>

              <p class="mt-5">
                <a class="btn btn-dark" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false"
                  aria-controls="collapseExample">
                  <i class="fas fa-chevron-circle-down"></i>
                </a>
              </p>
              <div class="collapse" id="collapse">
                <div class="card card-body">
                  Création d'un mini jeu console avec le language C#
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>