<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

    <div class="container">
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col-12">
                    <h2 class="pt-9 mb-5 rounded shadow">
                        <i class="fas fa-cogs"></i> Compétences
                    </h2>
                    <h2 class="mb-5"> Techniques </h2>
                    <dl class="row">
                        <dt class="col-sm-3"> HTML <i class="fab fa-html5"></i> / CSS <i class="fab fa-css3"></i></dt>
                        <dd class="col-sm-9">Utilisation du HTML pour de nombreux projets ainsi que le CSS (en début de
                            parcours)</dd>
                        <dt class="col-sm-3"> PHP <i class="fab fa-php"></i></dt>
                        <dd class="col-sm-9">Apprentissage de la programmation orientée objet avec PHP. Création de
                            plusieurs
                            site web (librairie de film, portfolio).
                            Découverte des API Rest avec le logiciel Postman</dd>
                        <dt class="col-sm-3"> Python <i class="fab fa-python"></i></dt>
                        <dd class="col-sm-9">Initiation à python. Création d'un site web (clone de Pinterest)</dd>
                        <dt class="col-sm-3 text-truncate"> Javascript <i class="fab fa-js-square"></i></dt>
                        <dd class="col-sm-9">Cours de Javascript avec Angular.</dd>
                        <dt class="col-sm-3"> Bootstrap <i class="fab fa-bootstrap"></i></dt>
                        <dd class="col-sm-9">Utilisation de Bootstrap pour tous les projets web</dd>
                        <dt class="col-sm-3"> Angular <i class="fab fa-angular"></i></dt>
                        <dd class="col-sm-9">Découverte d'Angular avec la création d'un nouveau portfolio</dd>
                        <dt class="col-sm-3"> Flask <i class="fab fa-affiliatetheme"></i></dt>
                        <dd class="col-sm-9">Utilisation de Flask pour la création d'un site web. Je suis un tutoriel en
                            anglais
                            pour apprendre le fonctionnement de ce framework</dd>
                        <dt class="col-sm-3"> PhpMyAdmin <i class="fas fa-database"></i></dt>
                        <dd class="col-sm-9">Utilisation de ce logiciel pour gérer toutes mes bases de données.</dd>
                    </dl>
                    <h2 class="mb-5 pt-9"> Systèmes </h2>
                    <dl class="row">
                        <dt class="col-sm-3 mb-5"> Linux <i class="fab fa-linux"></i></dt>
                        <dd class="col-sm-9  ">Installation de Linux en sous-réseaux windows avec Débian. Par la suite
                            j'ai
                            configurer une virtualbox afin d'utiliser Linux.
                            Apprentissage des regex.</dd>
                    </dl>

                </div>
            </div>

        </div>
    </div>

</body>

</html>