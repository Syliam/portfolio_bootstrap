<?php 
//on inclue toutes nos classes ! 
spl_autoload_register(); 


//on instancie notre contrôleur où sont toutes nos méthodes
//pour chaque page
include("DbConnection.php");
include("Controller.php");
include("Manager.php");
include("Tools.php");


//nouveau controller
$controller = new Controller();

//nouveau tools
$tools = new Tools();
$tools->initConfig();


//rootage des pages du site
if(empty($_GET['page']) OR $_GET['page'] =='accueil'){
    $controller->accueil();
}
elseif ($_GET['page'] =='recommandation'){
    $controller->recommandation();
}

else{
    $controller->error();
}

?>