<?php
class Tools
{
    //vérification nom
    /**
     * retourne une chaine de caractère si le nom n'est pas conforme
     * retourne vide si le nom est conforme
     */
    private function verifNom($nom)
    {
        if(empty($nom)) {
            return "Veuillez renseigner votre nom de famille !";
        }
        elseif (mb_strlen($nom) <= 1) {
            return "Votre nom de famille est court. Veuillez le rallonger !";
        }elseif (mb_strlen($nom) > 75) {
            return "Votre nom de famille est trop long !";
        }
        
        
    }

    /**
     * retourne une chaine de caractère si le prénom n'est pas conforme
     * retourne vide si le prénom est conforme
     */
    private function verifPrenom($prenom)
    {
            if(empty($prenom)) {
                return "Veuillez renseigner votre prénom !";
            }
            elseif (mb_strlen($prenom) <= 1) {
                return "Votre prénom est court. Veuillez le rallonger !";
            }elseif (mb_strlen($prenom) > 75) {
                return "Votre prénom est trop long !";
            }
    }

    /**
     * retourne une chaine de caractère si le métier n'est pas conforme
     * retourne vide si le métier est conforme
     */
    private function verifMetier($metier)
    {
            if(empty($metier)){
                return"Veuillez renseinger votre métier !";
            }
            elseif (mb_strlen($metier) <= 1) {
                return "Votre métier est court. Veuillez le rallonger !";
            }elseif (mb_strlen($metier) > 75) {
                return "Votre métier est trop long !";
            }
    }

    /**
     * retourne une chaine de caractère si l'entreprise n'est pas conforme
     * retourne vide si l'entreprise' est conforme
     */
    private function verifEntreprise($entreprise)
    {
            if(empty($entreprise)){
                return "Veuillez renseinger votre entreprise !";
            }
            elseif (mb_strlen($entreprise) <= 1) {
                return "Le nom de votre entreprise est trop court. Veuillez le rallonger !";
            }elseif (mb_strlen($entreprise) > 75) {
                return "Le nom de votre entreprise est trop long ! ";
            }
    }

    /**
     * retourne une chaine de caractère si le message n'est pas conforme
     * retourne vide si le message est conforme
     */
    private function verifMsg($msg)
    {
            if(empty($msg)){
                return "Veuillez renseigner votre message !";
            }
            elseif (mb_strlen($msg) <= 1) {
                return "Votre message est trop court. Veuillez le rallonger !";
            }elseif (mb_strlen($msg) > 10000) {
                return "Votre message est trop long ! ";
            }
    }

    /**
     * retourne une chaine de caractère si l'email n'est pas conforme
     * retourne vide si l'email est conforme
     */
    private function verifEmail($email)
    {
            if(empty($email)){
                return "L'email est requis !";
            }
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                return "Votre email n'est pas valide !";
            }
            elseif (mb_strlen($email) <= 3){
                return "Votre email est trop court !";
            }
            elseif(mb_strlen($email) >200 ){
                return "Votre email est trop long !";
            }
    }

    /**
     * retourne un tableau d'erreurs des paramètres
     * retourne vide s'il n'y a pas d'erreurs
     */
    public function recommandationFormHasErrors($nom, $prenom, $metier, $entreprise, $msg){
        //resultat de la fonction
        $errors=[];

        $errorNom = $this->verifNom($nom);
        $errorPrenom = $this->verifPrenom($prenom);
        $errorMetier = $this->verifMetier($metier);
        $errorEntreprise = $this->verifEntreprise($entreprise);
        $errorMessage = $this->verifMsg($msg);

        if(!empty($errorNom)){
            $errors[] = $errorNom;
        }

        if(!empty($errorPrenom)){
            $errors[] = $errorPrenom;
        }

        if(!empty($errorMetier)){
            $errors[] = $errorMetier;
        }

        if(!empty($errorEntreprise)){
            $errors[] = $errorEntreprise;
        }

        if(!empty($errorMessage)){
            $errors[] = $errorMessage;
        }

        return $errors;
    }

    /**
     * retourne un tableau d'erreurs des paramètres
     * retourne vide s'il n'y a pas d'erreurs
     */
    public function contactFormHasErrors($nom, $prenom, $email, $msg)
    {
        $errors=[];
        $errorNom = $this->verifNom($nom);
        $errorPrenom = $this->verifPrenom($prenom);
        $errorEmail = $this->verifEmail($email);
        $errorMessage = $this->verifMsg($msg);

        if(!empty($errorNom)){
            $errors[] = $errorNom;
        }

        if(!empty($errorPrenom)){
            $errors[] = $errorPrenom;
        }

        if(!empty($errorEmail)){
            $errors[] = $errorEmail;
        }

        if(!empty($errorMessage)){
            $errors[] = $errorMessage;
        }

        return $errors;
    }


    public function refresh()
    {
        header ("Refresh: 3;URL=index.php?page=accueil");
    }

    public function initConfig(){
        $jsonfileContent = file_get_contents("./server.config.json");
        $data = json_decode($jsonfileContent,true);
        
        define("DBHOST", $data["DBHOST"]);  //soit localhost, soit l'IP du serveur
        define("DBUSER", $data["DBUSER"]);       //utilisateur de la base (différent de PHPmyAdmin)
        define("DBPASS", $data["DBPASS"]);           //mot de passe
        define("DBNAME", $data["DBNAME"]);  //nom de la base de données

    }

    
 
}
 